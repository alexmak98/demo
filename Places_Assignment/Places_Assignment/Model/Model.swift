//
//  Model.swift
//  Places_Assignment
//
//  Created by User on 23.06.2022.
//

import Foundation

struct Places: Codable {
    
    var place: [Place]
    var total: Int
    
}

struct Place: Codable {
    
    var alias: String
    var name: String
    var longitude: Double
    var latitude: Double
    var description: String
    var icon: String
    
}
