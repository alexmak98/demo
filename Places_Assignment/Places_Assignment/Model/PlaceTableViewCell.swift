//
//  PlaceTableViewCell.swift
//  Places_Assignment
//
//  Created by User on 23.06.2022.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var networkService: NetworkService?
    var iconAPIURL = "https://secure.closepayment.com/close-admin/1.0"
    
    func updateWithData(place: Place){
        
        guard let imageURL = URL(string: iconAPIURL + place.icon) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: imageURL) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data)
            else {
                return
            }
            DispatchQueue.main.async {
                self.iconImageView.image = image
            }
        }
        task.resume()
        
        nameLabel.text = place.name
        descriptionLabel.text = place.description
        self.accessoryType = .detailButton
    }
    
    override func prepareForReuse() {
        nameLabel.text = ""
        descriptionLabel.text = ""
        iconImageView.image = nil
    }
    
}
