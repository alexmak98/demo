//
//  ViewModel.swift
//  Places_Assignment
//
//  Created by User on 23.06.2022.
//

import Foundation
import UIKit

class ViewModel {
    
    var networkService: NetworkService
    var getDataCallback: (([Place]) -> Void)?
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    func fetchPlaces() {
        networkService.fetchPlaces { result in
            switch result {
            case .success(let places):
                self.getDataCallback?(places)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
