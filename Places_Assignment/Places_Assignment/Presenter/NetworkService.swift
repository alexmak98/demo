//
//  NetworkService.swift
//  Places_Assignment
//
//  Created by User on 23.06.2022.
//

import UIKit

enum NetworkError: Error {
    case connectionError
}

class NetworkService {
    
    func fetchPlaces(completion: @escaping (Result<[Place], NetworkError>) -> Void){
        let placesURL = "https://secure.closepayment.com/close-admin/1.0/place/meappid?meAppId=50&records=42"
        guard let url = URL(string: placesURL) else {
            completion(.failure(NetworkError.connectionError))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200,
                  let data = data else {
                      completion(.failure(NetworkError.connectionError))
                      return
            }
            
            if let places = try? JSONDecoder().decode(Places.self, from: data) {
                completion(.success(places.place))
            } else {
                completion(.failure(NetworkError.connectionError))
            }
        }
        task.resume()
    }
    
}
