//
//  PlacesTableViewController.swift
//  Places_Assignment
//
//  Created by User on 23.06.2022.

import UIKit

class PlacesTableViewController: UITableViewController {

    var viewModel = ViewModel(networkService: NetworkService())
    var places = [Place]()

    override func viewDidLoad() {
        
        viewModel.getDataCallback = { places in
            self.places = places
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        viewModel.fetchPlaces()
    }
    
}
    
extension PlacesTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath) as? PlaceTableViewCell else {
            return UITableViewCell()
        }
        
        cell.updateWithData(place: places[indexPath.row])
        
        return cell
    }
}

extension PlacesTableViewController {

    override func tableView(_ tableView: UITableView,
                            accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        let locationVC = LocationViewController.instantiate()
        let location = places[indexPath.row]

        locationVC.updateWithLocation(location, url: location.icon)

        present(locationVC, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
