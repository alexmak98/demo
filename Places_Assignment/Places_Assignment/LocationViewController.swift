//
//  LocationViewController.swift
//  Places_Assignment
//
//  Created by User on 24.06.2022.
//

import UIKit

class LocationViewController: UIViewController, Storyboarded {
    
    var location: Place?
    var iconImage: UIImage?
    var iconAPIURL = "https://secure.closepayment.com/close-admin/1.0"
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var urlTextView: UITextView!
    
    func updateWithLocation(_ location: Place, url: String){
        
        guard let imageURL = URL(string: iconAPIURL + url) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: imageURL) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data)
            else {
                return
            }
            self.iconImage = image
            self.location = location
            self.updateUI()
        }
        task.resume()
        
    }
    
    func updateUI(){
        
        guard let location = location,
              let iconImage = iconImage else {
            return
        }
        
        let messageText = """
            This places is located at:
            lat: \(location.latitude), long: \(location.longitude)
            For more information, please visit
            """
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        guard let font = UIFont(name: "Helvetica", size: 20) else { return }
        
        let attributedString = NSMutableAttributedString(string: "no website in JSON")
        
        attributedString.addAttributes(
            [.link: "https://www.google.com",
             .paragraphStyle: paragraph,
             .font: font],
            range: NSRange(location: 0, length: attributedString.length))
        
        DispatchQueue.main.async {
            self.nameLabel.text = location.name
            self.descriptionLabel.text = "\(location.description)\n\(messageText)"
            self.iconImageView.image = iconImage
            self.urlTextView.attributedText = attributedString
        }
    }
    
}
